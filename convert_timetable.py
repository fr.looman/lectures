from pathlib import Path
import re
import json

import dateutil
import ics

course = ics.Calendar(Path('mytimetable_export.ics').read_text())

events = []

def parse_data(description):
    type_conversion = {
        'Exam/Test in Osiris': 'Exam',
        'Exam/Test not in Osiris': 'Minitest',
        'Lecture': 'Lecture',
        'Instruction': 'Exercises',
    }
    type = type_conversion[re.search(
        '^Type: (.*)$', description, flags=re.MULTILINE
    ).group(1)]
    out = re.search(
        '^Location\\(s\\):\n(?P<name>.*?)(?:: http:.*?)?$',
        description,
        flags=re.MULTILINE,
    ).groupdict()
    out['type'] = type
    return out


for original in course.events:
    parsed = parse_data(original.description)
    event = {
        'start': str(original.begin),
        'end': str(original.end),
        'title': f'{parsed["type"]} @ {parsed["name"]}',
    }
    events.append(event)

events = sorted(events, key=(lambda event: dateutil.parser.isoparse(event['start'])))
events[-1]['title'] = events[-1]['title'].replace('Exam', 'Retake exam')
Path('src/figures/events.json').write_text(json.dumps(events))
