```python tags=["initialize"]
from matplotlib import pyplot as plt 
import numpy as np
from math import pi
```
# Solutions for lecture 9 exercises

## Warm-up exercises
#### Question 1.

A primitive unit cell is a unit cell which contains a single lattice point.
Because it only contains a lattice point, it is the unit cell with the smallest volume out of all unit cells.

#### Question 2.

Let us first consider the FCC lattice.
Its primitive lattice vectors vectors are
\begin{align}
\mathbf{a}_1 &= \frac{a}{2}(\mathbf{\hat{x}} + \mathbf{\hat{y}})\\
\mathbf{a}_2 &= \frac{a}{2}(\mathbf{\hat{x}} + \mathbf{\hat{z}})\\
\mathbf{a}_3 &= \frac{a}{2}(\mathbf{\hat{y}} + \mathbf{\hat{z}}).
\end{align}
With respect to the conventional unit cell, the basis in fractional coordinates is $\bigcirc(1/2,1/2,0)$, $\bigcirc(1/2,0,1/2)$, $\bigcirc(0,1/2,1/2)$ and $\bigcirc(0,0,0)$.
With respect to the primitive unit cell, the basis is $\bigcirc(0,0,0)$.
Let us now consider the BCC lattice.
The primitive lattice vectors are
\begin{align}
\mathbf{a}_1 &= \frac{a}{2}(\mathbf{\hat{x}} + \mathbf{\hat{y}} + \mathbf{\hat{z}})\\
\mathbf{a}_2 &= a\mathbf{\hat{x}}\\
\mathbf{a}_3 &= a\mathbf{\hat{y}}.
\end{align}
The basis of the conventional unit cell is $\bigcirc(0,0,0)$ and $\bigcirc(1/2,1/2,1/2)$.
For the primitive unit cell the basis is $\bigcirc(0,0,0)$.

#### Question 3.

You would need at least two. A diatomic crystal could require more than two basis vectors if the proportion between the two elements is not one to one.

#### Question 4.

$$
F = \frac{\pi}{6}
$$

#### Question 5.

See lecture notes

## Exercise 1: Diatomic crystal¶

#### Question 1.

```python
y = np.repeat(np.arange(0,8,2),4)
x = np.tile(np.arange(0,8,2),4)
plt.figure(figsize=(5,5))
plt.axis('off')

# WZ
plt.plot([5,5,7,7,5],[5,7,7,5,5], color='k',ls=':')
plt.annotate('WZ',(6,6.5),fontsize=14,ha='center')

# PUC1 
plt.plot([0,2,4,2,0],[4,6,6,4,4], color='k',ls=':')

# UPC2
plt.plot([6,4,2,4,6],[0,0,2,2,0], color='k',ls=':')

plt.plot(x,y,'ko', markersize=15)
plt.plot(x+1,y+1, 'o', markerfacecolor='none', markeredgecolor='k', markersize=15);
```
#### Question 2.

The area of the primitive unit cell is $A = a^2$. 
If the filled and empty circles are identical particle, the nearest neighbour distance becomes $a^* = \frac{a}{\sqrt{2}}$ and thus the area $A^* = {a^*}^2 = \frac{a^2}{2} = \frac{A}{2}$.

#### Question 3.

One set of primitive lattice vectors is
$$
\mathbf{a_1} = a \hat{\mathbf{x}}, \quad \mathbf{a_2} = a \hat{\mathbf{y}}.
$$
With respect to the primitive lattice vectors, the basis is
$
\huge \bullet \normalsize(0,0), \quad \bigcirc(\frac{1}{2},\frac{1}{2}).
$

#### Question 4.

The lattice is a cubic lattice. 
The basis of the crystal is

$
\huge \bullet \normalsize(0,0,0), \quad \bigcirc(\frac{1}{2},\frac{1}{2},\frac{1}{2}).
$

An example of such a material is Cesium Chloride (CsCl)

#### Question 5.

We obtain the BCC lattice.

Example: Sodium (Na)

#### Question 6.

$$
F = \frac{\sqrt(3)\pi}{8}
$$
    
## Exercise 2: Diamond lattice
#### Question 1.

The conevtional unit cell of diamond consists out of two shifted fcc lattices.
One set of primitive lattice vectors is
\begin{align}
\mathbf{a_1} &= \frac{a}{2} \left(\hat{\mathbf{x}}+\hat{\mathbf{y}} \right) \\
\mathbf{a_2} &= \frac{a}{2} \left(\hat{\mathbf{x}}+\hat{\mathbf{z}} \right) \\
\mathbf{a_3} &= \frac{a}{2} \left(\hat{\mathbf{y}}+\hat{\mathbf{z}} \right).
\end{align}
The volume of the primitive unit cell is
$$
V = \left| \mathbf{a_1} \cdot \left(\mathbf{a_2} \times \mathbf{a_3} \right) \right| = \frac{a^3}{4}
$$

#### Question 2.

The primitive unit cell contains 2 atoms.
With respect to the set of primitive lattcie vectors, the basis is $ \mathrm{C}(0,0,0)$ and $\mathrm{C}(\frac{1}{4},\frac{1}{4},\frac{1}{4})$.

#### Question 3.

One FCC lattice contains 4 atom. 
Because the diamond conventional unit cell contains two shifted FCC lattices, it will contain 8 atoms.
The volume of the conventional unit cell is $V = a^3$.

#### Question 4.


??? hint "Visual hint"

    Consider the atom at (0.25,0.25,0.25) coordinates in the interactive diamond lattice image and look at its neighbours.

The distance between neirest neighbouring atoms is $d = \frac{\sqrt{3}a}{4}$

#### Question 5.

$$
F = \frac{\sqrt{3}\pi}{16}
$$
    

## Exercise 3: Directions and Spacings of Miller planes
#### Question 1.

Miller plane is a plane that intersects an infinite number of lattice points.
Miller indeces are a set of 3 integers which specify a set of parallel planes.

#### Question 2.

??? hint "Small hint"

    The $(hkl)$ plane intersects lattice at position vectors of $\frac{\mathbf{a_1}}{h}, \frac{\mathbf{a_2}}{k}, \frac{\mathbf{a_1}}{l}$. 
    Can you define two general vector inside the $(hkl)$ plane that span the plane? 

#### Question 3.

The same hint as mentioned in the previous subquestion also applies here.

#### Question 4

??? hint "Big hint"

    There is always a neighbouring lattice plane which intersects the (0,0,0) lattice point.

??? hint "Small hint"

    Do not forget to normalize your unit vector.
