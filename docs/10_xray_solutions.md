```python tags=["initialize"]
from matplotlib import pyplot as plt 
import numpy as np
from math import pi
```
# Solutions for lecture 10 exercises

## Warm-up exercises
#### Question 1.
Hint: you can make use of the [scalar triple product](https://en.wikipedia.org/wiki/Triple_product#Scalar_triple_product).
#### Question 2.
If $\mathbf{k}-\mathbf{k'}\neq \mathbf{G}$, then the argument of the exponent has a phase factor dependent on the real-space lattice points.
Because we sum over each of these lattice points, each argument has a different phase.
Summing over all these phases results in an average amplitude of 0, resulting in no intensity peaks.
#### Question 3.
No, there is a single atom, and thus only one term in the structure factor. 
This results in only a single exponent being present in the structure factor, which is always nonzero.
#### Question 4.
No, an increase of the unit cell size cannot create new diffraction peaks (see lecture).

## Exercise 1: Equivalence of direct and reciprocal lattice

#### Question 1.
Solution

$$
V^*=\left|\mathbf{b}_{1} \cdot\left(\mathbf{b}_{2} \times \mathbf{b}_{3}\right)\right| = \frac{2\pi}{V}\left| (\mathbf{a}_{2} \times \mathbf{a}_{3}) \cdot\left(\mathbf{b}_{2} \times \mathbf{b}_{3}\right)\right| = \frac{(2\pi)^3}{V}
$$

In the second equality, we used the reciprocal lattice vector definition (see notes). 
In the third equality, we used the identity:

$$
(\mathbf{a} \times \mathbf{b}) \cdot(\mathbf{c} \times \mathbf{d})=(\mathbf{a} \cdot \mathbf{c})(\mathbf{b} \cdot \mathbf{d})-(\mathbf{a} \cdot \mathbf{d})(\mathbf{b} \cdot \mathbf{c})
$$

#### Question 2.
Because the relation between direct and reciprocal lattice is symmetric, so are the expressions for the direct lattice vectors through the reciprocal ones:

$$
\mathbf{a}_{i} \epsilon_{ijk} = \frac{2\pi}{V^*} (\mathbf{b}_{j} \times \mathbf{b}_{k})
$$

where $\epsilon_{ijk}$ is the [Levi-Civita tensor](https://en.wikipedia.org/wiki/Levi-Civita_symbol#Three_dimensions)

#### Question 3.
One set of the BCC primitive lattice vectors is given by:

$$
\mathbf{a_1} = \frac{a}{2} \left(-\hat{\mathbf{x}}+\hat{\mathbf{y}}+\hat{\mathbf{z}} \right) \\
\mathbf{a_2} = \frac{a}{2} \left(\hat{\mathbf{x}}-\hat{\mathbf{y}}+\hat{\mathbf{z}} \right) \\
\mathbf{a_3} = \frac{a}{2} \left(\hat{\mathbf{x}}+\hat{\mathbf{y}}-\hat{\mathbf{z}} \right).
$$

From this, we find the following set of reciprocal lattice vectors:

$$
\mathbf{b_1} = \frac{2 \pi}{a} \left(\hat{\mathbf{y}}+\hat{\mathbf{z}} \right) \\
\mathbf{b_2} = \frac{2 \pi}{a} \left(\hat{\mathbf{x}}+\hat{\mathbf{z}} \right) \\
\mathbf{b_3} = \frac{2 \pi}{a} \left(\hat{\mathbf{x}}+\hat{\mathbf{y}} \right),
$$

which forms a reciprocal FCC lattice.
The opposite relation follows directly from our previous result.

#### Question 4.
Because the 1st Brillouin Zone is the Wigner-Seitz cell of the reciprocal lattice, we need to construct the Wigner-Seitz cell of the FCC lattice.
For visualization, it is convenient to look at [FCC lattice](https://solidstate.quantumtinkerer.tudelft.nl/9_crystal_structure/#face-centered-cubic-lattice) introduced in the previous lecture and count the neirest neighbours of each lattice point.
We see that each lattice point contains 12 neirest neighbours and thus the Wigner-Seitz cell contains 12 sides!

## Exercise 2: Miller planes and reciprocal lattice vectors

#### Question 1.
Hints

??? hint "First small hint"

    The $(hkl)$ plane intersects lattice at position vectors of $\frac{\mathbf{a_1}}{h}, \frac{\mathbf{a_2}}{k}, \frac{\mathbf{a_3}}{l}$. 
    Can you define a general vector inside the $(hkl)$ plane? 

??? hint "Second small hint"

    Whats the best vector operation to show orthogonality between two vectors? 

#### Question 2.
One can compute the normal to the plane by using result from Subquestion 1:

$\hat{\mathbf{n}} = \frac{\mathbf{G}}{|G|}$

Let us consider a very simple case in which we have the miller planes $(h00)$.
For lattice planes, there is always a plane intersecting the zero lattice point (0,0,0). 
As such, the distance from this plane to the closest next one is given by:

$ d = \hat{\mathbf{n}} \cdot \frac{\mathbf{a_1}}{h} = \frac{2 \pi}{|G|} $

#### Question 3.
Since $\rho=d / V$, we must maximize $d$. 
To do that, we minimize must $|G|$. 
Therefore the smallest possible reciprocal lattice vectors are the (100) family of planes (in terms of FCC primitive lattice vectors).

## Exercise 3: X-ray scattering in 2D

#### Question 1.
See figure

```python
def reciprocal_lattice(N = 7, lim = 40):
    y = np.repeat(np.linspace(-18.4*(N//2),18.4*(N//2),N),N)
    x = np.tile(np.linspace(-13.4*(N//2),13.4*(N//2),N),N)

    plt.figure(figsize=(5,5))

    plt.plot(x,y,'o', markersize=10, markerfacecolor='none', color='k')
    plt.xlim([-lim,lim])
    plt.ylim([-lim,lim])
    plt.xlabel('$\mathbf{b_1}$')
    plt.ylabel('$\mathbf{b_2}$')
    plt.xticks(np.linspace(-lim,lim,5))
    plt.yticks(np.linspace(-lim,lim,5))
    
reciprocal_lattice()
```

#### Question 2.
Since we have elastic scattering, we obtain
$|\mathbf{k}| = |\mathbf{k}'| = \frac{2 \pi}{\lambda} = 37.9 nm^{-1}$
#### Question 3.
See figure

```python
reciprocal_lattice()
# G vector
plt.arrow(0,0,13.4*2,18.4,color='r',zorder=10,head_width=2,length_includes_head=True)
plt.annotate('$\mathbf{G}$',(17,6.5),fontsize=14,ha='center',color='r')
# k vector
plt.arrow(-6,37.4,6,-37.4,color='b',zorder=11,head_width=2,length_includes_head=True)
plt.annotate('$\mathbf{k}$',(-8,18),fontsize=14, ha='center',color='b')
# k' vector
plt.arrow(-6,37.4,6+13.4*2,-37.4+18.4,color='k',zorder=11,head_width=2,length_includes_head=True)
plt.annotate('$\mathbf{k\'}$',(15,30),fontsize=14, ha='center',color='k');
```

## Exercise 4: Structure factors

#### Question 1.
$S(\mathbf{G}) = \sum_j f_j e^{i \mathbf{G} \cdot \mathbf{r_j}} = f(1 + e^{i \pi (h+k+l)})$
#### Question 2.
Solving for $h$, $k$, and $l$ results in 

$$
S(\mathbf{G}) = \begin{cases}
    2f, \: \text{if $h+k+l$ is even}\\
    0, \: \text{if $h+k+l$ is odd}.
\end{cases}
$$

Thus if $h+k+l$ is odd, diffraction peaks dissapear

#### Question 3.
Let $f_1 \neq f_2$, then

$
S(\mathbf{G}) = \begin{cases}
f_1 + f_2, \text{if $h+k+l$ is even}\\
f_1 - f_2, \text{if $h+k+l$ is odd}
\end{cases}       
$


#### Question 4.
Due to bcc systematic absences, the peaks from lowest to largest angle are:
$(110),(200),(211), (220), (310)$

#### Question 5.
$a = 2.9100$Å
