# Periodic table of Fermi surfaces

Data taken from the [Fermi surface database](https://www.phys.ufl.edu/fermisurface/), © 2000 Tat-Sang Choy, CC-BY.

<script>
function plotlyRender() {
    Plotly.d3.json(
        `../fermi_surface_plots/${document.getElementById('element_selector').value}.json`,
        function(error, fig) {
            Plotly.newPlot('elements_plot', fig.data, fig.layout)
        }
    );
}
</script>
<select id="element_selector" onchange="plotlyRender()">
<option value="Ac">Ac</option>
<option value="Ag">Ag</option>
<option value="Al">Al</option>
<option value="Au" selected>Au</option>
<option value="Ba">Ba</option>
<option value="Be">Be</option>
<option value="Ca">Ca</option>
<option value="Cd">Cd</option>
<option value="Cd_fcc">Cd_fcc</option>
<option value="Co_fcc">Co_fcc</option>
<option value="Co_fccdn">Co_fccdn</option>
<option value="Co_fccup">Co_fccup</option>
<option value="Co_hcp">Co_hcp</option>
<option value="Codn">Codn</option>
<option value="Coup">Coup</option>
<option value="Cr">Cr</option>
<option value="Cs">Cs</option>
<option value="Cu">Cu</option>
<option value="Fe_bcc">Fe_bcc</option>
<option value="Fedn">Fedn</option>
<option value="Feup">Feup</option>
<option value="Ga">Ga</option>
<option value="Hf">Hf</option>
<option value="In">In</option>
<option value="Ir">Ir</option>
<option value="K">K</option>
<option value="Li" selected>Li</option>
<option value="Mg">Mg</option>
<option value="Mn">Mn</option>
<option value="Mn_fcc">Mn_fcc</option>
<option value="Mo">Mo</option>
<option value="Na">Na</option>
<option value="Nb">Nb</option>
<option value="Ni_fcc">Ni_fcc</option>
<option value="Nidn">Nidn</option>
<option value="Niup">Niup</option>
<option value="Os">Os</option>
<option value="Pb">Pb</option>
<option value="Pd">Pd</option>
<option value="Pt">Pt</option>
<option value="Rb">Rb</option>
<option value="Re">Re</option>
<option value="Rh">Rh</option>
<option value="Ru">Ru</option>
<option value="Ru_fcc">Ru_fcc</option>
<option value="Sc">Sc</option>
<option value="Sc_bcc">Sc_bcc</option>
<option value="Sr">Sr</option>
<option value="Ta">Ta</option>
<option value="Tc">Tc</option>
<option value="Tc_fcc">Tc_fcc</option>
<option value="Th">Th</option>
<option value="Ti">Ti</option>
<option value="Ti_bcc">Ti_bcc</option>
<option value="Ti_fcc">Ti_fcc</option>
<option value="Tl">Tl</option>
<option value="V">V</option>
<option value="W">W</option>
<option value="Y">Y</option>
<option value="Y_fcc">Y_fcc</option>
<option value="Zn">Zn</option>
<option value="Zn_fcc">Zn_fcc</option>
<option value="Zr">Zr</option>
<option value="Zr_bcc">Zr_bcc</option>
<option value="Zr_fcc">Zr_fcc</option>
</select>
<div id="elements_plot" data-plotlySource="../fermi_surface_plots/Li.json" class="plotlyPlaceholder" style="min-height: 75%;"></div>