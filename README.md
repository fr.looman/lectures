# Solid state lectures

Lecture notes and teaching material used for the Delft University of Technology course Solid State Physics (Vaste Stof Fysica, TN2844).

The compiled materials are available at http://solidstate.quantumtinkerer.tudelft.nl

## How to compile
To compile it is advised to use a separate environment. We use miniconda for environment management, which can be found at https://docs.conda.io/en/latest/miniconda.html

First create a new conda environment named `lectures` with Python 3.6 and activate it. You can also use another environment manager of your choice.

```sh
conda create -n lectures python=3.6
conda activate lectures
```

Clone this repository and enter the directory.

```sh
git clone https://gitlab.kwant-project.org/solidstate/lectures.git
cd lectures
```
Install the required packages from `requirements.txt`.

```sh
pip install -r requirements.txt
```

And finally run `execute.py` to prepare the source files for `mkdocs`.

```sh
python execute.py
```

Now that all the files are where they should be, you can either run a development server or build the files for deployment using `mkdocs`. The commands to achieve this are:

```sh
mkdocs serve  # Run a development server
mkdocs build  # Build for deployment
```
